export class Person {
    key: string;
    firstname: string;
    lastname: string;
    job: string;
    salary: number;
}
