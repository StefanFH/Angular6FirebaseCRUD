import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Person } from './person';
import { map } from 'rxjs/operators';
import { Observable, of  } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private persons: AngularFireList<any>;

  personsObservable$: Observable<any[]>;

  constructor(private db: AngularFireDatabase) {
    this.persons = this.db.list('/persons');


   }
 
  addPerson(person: Person[]) {   
    this.persons.push(person);
  }
 
  getPersons() {
    var personList = new Array<Person>();
    this.persons.snapshotChanges().forEach( person => {
      person.forEach( personData =>{
        var person = new Person();
        person = (personData.payload.val()) as Person;
        person.key = personData.key;
        personList.push(person);
        });
    });
    return personList;
    //return this.persons.snapshotChanges();
  }

  deletePerson(person: Person){
    this.persons.remove(person.key);
  }

  updatePerson(person: Person) {  
    console.log('update ' + person.key); 
    this.db.object('/persons/' + person.key).set(person);
  }
}
