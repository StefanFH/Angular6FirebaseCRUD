import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Person } from '../person';

@Component({
  selector: 'app-editperson',
  templateUrl: './editperson.component.html',
  styleUrls: ['./editperson.component.css']
})
export class EditpersonComponent implements OnInit {

  description:string;

  person: Person;

  constructor(
        private dialogRef: MatDialogRef<EditpersonComponent>,
        @Inject(MAT_DIALOG_DATA) data) {                
        this.description = data.description;
        if(data != undefined)
          this.person = data as Person;
        else
          this.person = new Person();
  }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close(this.person);
  }

  close() {
      this.dialogRef.close();
  }
}