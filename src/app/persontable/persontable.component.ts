import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatDialogConfig, MatDialog } from '@angular/material';
import { PersontableDataSource } from './persontable-datasource';
import { EditpersonComponent } from '../editperson/editperson.component';
import { Person } from '../person';
import { AngularFireDatabase } from 'angularfire2/database';
import { PersonService } from '../person.service';
import { of } from 'rxjs';

@Component({
  selector: 'persontable',
  templateUrl: './persontable.component.html',
  styleUrls: ['./persontable.component.css']
})
export class PersontableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  dataSource: PersontableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['actions', 'firstname', 'lastname', 'job', 'salary'];

  ngOnInit() {
    this.dataSource = new PersontableDataSource(this.personService.getPersons(), this.paginator, this.sort);
  }


  constructor(private personService: PersonService, private db: AngularFireDatabase, private dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef){
  }


  openDialog(person: Person) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    if(person == null || person == undefined)
      person = new Person();
    dialogConfig.data = person;
    var dialogReference = this.dialog.open(EditpersonComponent, dialogConfig);
    dialogReference.afterClosed()
    .subscribe(selection => {
      if (selection && selection != undefined)
      { 
        var tempPerson = selection as Person;
        if(tempPerson.key == undefined)
          this.personService.addPerson(selection);
        else
          this.personService.updatePerson(tempPerson);
        this.dataSource.data = this.personService.getPersons();
      }
      });
  }

  openNewPersonDialog() {
    this.openDialog(new Person());
  }

  deletePerson(person: Person){
    this.personService.deletePerson(person);
    this.dataSource.data = this.personService.getPersons();
  }
}
